import pymongo
import sys

def printHelp():
    print('''
    -addUser
    -listUsers
    -remove
    -update
    ''')

def addUser(name,surname,email,password):
    print("addUser()")
    client = pymongo.MongoClient("localhost", 27017)

    try:
        user = {
                "name":name,
                "surname":surname,
                "email":email,
                "password":password
                }
        cur = client.myusers.users.insert_one(user)
    finally:
        client.close()




def listUsers():
    print("listUsers{}")
    client = pymongo.MongoClient("localhost", 27017)

    try:
        users = []
        cur = client.myusers.users.find()
        for u in cur:
            users.append(u)
        for user in users:
            print(f"{user}\n")
    finally:
        client.close()

def updateUser(upemail, name,surname,password):
    print("updateUser()")
    client = pymongo.MongoClient("localhost", 27017)

    try:
        updates = {
                "name":name,
                "surname":surname,
                "password":password
                }

        cur = client.myusers.users.update_one({"email": upemail}, {"$set": updates})
    finally:
        client.close()

def removeUser(rmemail):
    print("rmUser")
    client = pymongo.MongoClient("localhost", 27017)

    try:
        cur = client.myusers.users.delete_one({"email":rmemail})
    finally:
        client.close()

print(sys.argv)
if len(sys.argv) < 2:
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    print("User added")

elif sys.argv[1] == "listUser":
    listUsers()

elif sys.argv[1]=="updateUser":
    upemail = sys.argv[2]
    updateUser(upemail,sys.argv[3],sys.argv[4],sys.argv[5])
    print("user updated")

elif sys.argv[1]=="removeUser":
    removeUser(sys.argv[2])
    print("user removed")
else: printhelp()
