import pymongo
import sys

def printHelp():
    print('''
    -addUser name surname email password
    -listUsers
    -remove email
    -update email name surname password
    -joinContact userEmail contactEmail
    -leaveContact userEmail contactEmail
    -listContacts userEmail
    ''')

def addUser(name, surname, email, password):
    print("addUser()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        user = {
            "name": name,
            "surname": surname,
            "email": email,
            "password": password,
            "contacts": []
        }
        cur = client.myusers.users.insert_one(user)
    finally:
        client.close()

def listUsers():
    print("listUsers()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        users = []
        cur = client.myusers.users.find()
        for u in cur:
            users.append(u)
        for user in users:
            print(f"{user}\n")
    finally:
        client.close()

def updateUser(upemail, name, surname, password):
    print("updateUser()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        updates = {
            "name": name,
            "surname": surname,
            "password": password
        }
        cur = client.myusers.users.update_one({"email": upemail}, {"$set": updates})
    finally:
        client.close()

def removeUser(rmemail):
    print("rmUser()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        cur = client.myusers.users.delete_one({"email": rmemail})
    finally:
        client.close()

def joinContact(userEmail, contactEmail):
    print("joinContact()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        cur = client.myusers.users.update_one(
            {"email": userEmail},
            {"$push": {"contacts": contactEmail}} 
        )
        
    finally:
        client.close()

def leaveContact(userEmail, contactEmail):
    print("leaveContact()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        cur = client.myusers.users.update_one(
            {"email": userEmail},
            {"$pull": {"contacts": contactEmail}}
        )
        
    finally:
        client.close()

def listContacts(userEmail):
    print("listContacts()")
    client = pymongo.MongoClient("localhost", 27017)
    try:
        user = client.myusers.users.find_one({"email": userEmail})
        if not user:
            raise Exception("User not found")
        contacts = user.get("contacts", [])
        for contact in contacts:
            print(contact)
    finally:
        client.close()

# Controlador principal
print(sys.argv)
if len(sys.argv) < 2:
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("User added")
elif sys.argv[1] == "listUsers":
    listUsers()
elif sys.argv[1] == "updateUser":
    updateUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("User updated")
elif sys.argv[1] == "removeUser":
    removeUser(sys.argv[2])
    print("User removed")
elif sys.argv[1] == "joinContact":
    joinContact(sys.argv[2], sys.argv[3])
    print("Contact added")
elif sys.argv[1] == "leaveContact":
    leaveContact(sys.argv[2], sys.argv[3])
    print("Contact removed")
elif sys.argv[1] == "listContacts":
    listContacts(sys.argv[2])
else:
    printHelp()

