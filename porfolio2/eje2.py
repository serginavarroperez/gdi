import redis
import uuid
import jwt
import json
import sys

def printHelp():
    print('''
    -addUser name surname email pass
    -listUsers query
    -update email name surname password
    -remove email
    ''')

def addUser(name,surname,email,password):
    print("addUser()")
    r = redis.Redis(host='localhost', port=6379, db=0)

    try:
        user = {
                "name":name,
                "surname":surname,
                "email":email,
                "password":password
                }
        data = json.dumps(user)
        r.set(f"/users/{user['email']}",data)#registrar clave
    finally:
        r.close()




def listUsers(query=""):
    print("listUsers{}")
    r = redis.Redis(host='localhost', port=6379, db=0)

    try:
        users = []
        cur = r.keys("/users/*")
        for k in cur:
            data = r.get(k)
            user = json.loads(data) #como data esta en json este lo deserializa
            users.append(user)
        print(users)
    finally:
        r.close()

def updateUser(email, name, surname, password):
    print("updateUser()")
    r = redis.Redis(host='localhost', port=6379, db=0)

    try:
        key = f"/users/{email}"
        if r.exists(key):
            user = {
                "name": name,
                "surname": surname,
                "email": email,
                "password": password
            }
            data = json.dumps(user)
            r.set(key, data)
        else:
            raise Exception("User not found")
    finally:
        r.close()

def removeUser(email):
    print("removeUser()")
    r = redis.Redis(host='localhost', port=6379, db=0)

    try:
        key = f"/users/{email}"
        if r.exists(key):
            r.delete(key)
        else:
            raise Exception("User not found")

    finally:
        r.close()

print(sys.argv)
if len(sys.argv) < 2: 
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("User added")
elif sys.argv[1] == "listUser":
    listUsers()
elif sys.argv[1] == "updateUser":
    updateUser(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    print("user updated")
elif sys.argv[1] == "removeUser":
    rmemail = sys.argv[2];
    removeUser(rmemail);
    print("user removed")               
