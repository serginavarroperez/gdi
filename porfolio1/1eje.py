import mysql.connector
import sys

def printHelp():
    print('''
    -addUser name surname email password
    -listUser [<query>]
    ''')

def init():
    con = mysql.connector.connect(user="root", password="root")
    try:
        cur = con.cursor()

        cur.execute("CREATE DATABASE IF NOT EXISTS eje1")
        cur.execute("USE eje1")
        cur.execute('''CREATE TABLE IF NOT EXISTS Users (
            name CHAR(32) NOT NULL,
            surname CHAR(32) NOT NULL,
            email CHAR(32) NOT NULL,
            password CHAR(32) NOT NULL,
            PRIMARY KEY (email)
        )''')

        con.commit()
    except Exception as e:
        print("Error during database initialization:", e)
    finally:
        con.close()

init()

def addUser(name, surname, email, password):
    print("addUser()")
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM Users WHERE email='{email}'")
        if len(cur.fetchall()) > 0: 
            raise Exception("User already exists")
        
        sql = f"INSERT INTO Users (name, surname, email, password) VALUES ('{name}', '{surname}', '{email}', '{password}')"
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print("Error adding user:", e)
    finally:
        con.close()

def listUsers(query=""):
    print("listUsers()")
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        if len(query) == 0:
            cur.execute("SELECT * FROM Users")
        else:
            cur.execute("SELECT * FROM Users WHERE " + query)
        
        for row in cur.fetchall():
            print(row)

    except Exception as e:
        print("Error listing users:", e)
    finally:
        con.close()

print(sys.argv)
if len(sys.argv) < 2: 
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("User added")
elif sys.argv[1] == "listUser":
    listUsers()
else: 
    printHelp()

