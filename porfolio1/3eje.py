import mysql.connector
import sys

def printHelp():
    print('''
Usage: python3 users.py <command>
Available commands:
    - addUser <name> <surname> <email> <password>
    - listUser [<query>]
    - updateUser <upemail>
    - removeUser <rmemail>
    - addFriend <ownerEmail> <targetEmail>
    - rmFriend <ownerEmail> <targetEmail>
    - lsFriends <email>
''')

def init():
    con = mysql.connector.connect(user="root", password="root")
    try:
        cur = con.cursor()
        cur.execute("CREATE DATABASE IF NOT EXISTS eje1")
        cur.execute("USE eje1")
        cur.execute('''CREATE TABLE IF NOT EXISTS Users (
            name CHAR(32) NOT NULL,
            surname CHAR(32) NOT NULL,
            email CHAR(32) NOT NULL,
            password CHAR(32) NOT NULL,
            PRIMARY KEY (email)
        )''')

        cur.execute('''
            CREATE TABLE IF NOT EXISTS Friend (
                owner CHAR(64),
                target CHAR(64),
                FOREIGN KEY (owner) REFERENCES User(email) ON DELETE CASCADE,
                FOREIGN KEY (target) REFERENCES User(email) ON DELETE CASCADE,
                PRIMARY KEY (owner, target)
            );
        ''')
        con.commit()
        print("Database initialized.")
    except Exception as e:
        print("Error during initialization:", e)
    finally:
        cur.close()
        con.close()

init()

def addUser(name, surname, email, password):
    print("addUser()")
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM Users WHERE email='{email}'")
        if len(cur.fetchall()) > 0: 
            raise Exception("User already exists")
        
        sql = f"INSERT INTO Users (name, surname, email, password) VALUES ('{name}', '{surname}', '{email}', '{password}')"
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print("Error adding user:", e)
    finally:
        con.close()

def listUsers(query=""):
    print("listUsers()")
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        if len(query) == 0:
            cur.execute("SELECT * FROM Users")
        else:
            cur.execute("SELECT * FROM Users WHERE " + query)
        
        for row in cur.fetchall():
            print(row)

    except Exception as e:
        print("Error listing users:", e)
    finally:
        con.close()

def updateUser(upemail,user):
    if not "name" in user: raise Exception("Missing name")
    if not "surname" in user: raise Exception("Missing surname")
    if not "password" in user: raise Exception("Missing password")

    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM Users Where email = '{upemail}'")
        if not cur.fetchone():
            raise Exception("Usuario no existe")
        cur.execute(f"UPDATE Users SET name = '{user['name']}', surname = '{user['surname']}', password = '{user['password']}' WHERE email = '{upemail}'")
        con.commit()

    except Exception as e:
        print(e)
        raise e
    finally:
        con.close()
    return user


def removeUser(rmemail):
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM Users Where email = '{rmemail}'")
        if not cur.fetchone():
            raise Exception("Usuario no existe")
        cur.execute(f"DELETE FROM Users WHERE email = '{rmemail}'")
        con.commit()

    except Exception as e:
        print(e)
        raise e
    finally:
        con.close()

def addFriend(ownerEmail, targetEmail):
    con = mysql.connector.connect(user="root", password="root", database="eje1")
    try:
        cur = con.cursor()
        cur.execute("SELECT email FROM Users WHERE email IN (%s, %s)", (ownerEmail, targetEmail))
        results = cur.fetchall()
        if len(results) == 2:  
            cur.execute("INSERT INTO Friend (owner, target) VALUES (%s, %s)", (ownerEmail, targetEmail))
            con.commit()  
            print("Friend added successfully.")
        else:
            print("One or both users do not exist.")
    except Exception as e:
        print("Error adding friend:", e)
    finally:
        cur.close()
        con.close()

def listFriends(email):
    con = mysql.connector.connect(user='root', password='root', database='eje1')
    try:
        cur = con.cursor()
        cur.execute("SELECT target FROM Friend WHERE owner = %s", (email,))
        friends = cur.fetchall()
        if friends:
            print("Friends of", email, ":")
            for friend in friends:
                print(friend[0])
        else:
            print("No friends found for this user.")
    except Exception as e:
        print("Error listing friends:", e)
    finally:
        cur.close()
        con.close()

def removeFriend(ownerEmail, targetEmail):
    con = mysql.connector.connect(user='root', password='root', database='eje1')
    try:
        cur = con.cursor()
        cur.execute("DELETE FROM Friend WHERE owner = %s AND target = %s", (ownerEmail, targetEmail))
        con.commit()
        print("Friendship removed successfully.")
    except Exception as e:
        print("Error removing friendship:", e)
    finally:
        cur.close()
        con.close()

print(sys.argv)
if len(sys.argv) < 2: 
    printHelp()
elif sys.argv[1] == "addUser":
    addUser(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    print("User added")
elif sys.argv[1] == "listUser":
    listUsers(sys.argv[2])
elif sys.argv[1] == "updateUser":
    upemail = sys.argv[2]
    user = {
            "name":sys.argv[3],
            "surname":sys.argv[4],
            "password":sys.argv[5]
            }
    updateUser(upemail,user)
    print("user updated")
elif sys.argv[1] == "removeUser":
    rmemail = sys.argv[2];
    removeUser(rmemail);
    print("user removed")
elif sys.argv[1] == "addFriend":
    addFriend(sys.argv[2], sys.argv[3])
elif sys.argv[1] == "lsFriends":
    listFriends(sys.argv[2])
elif sys.argv[1] == "rmFriend":
    removeFriend(sys.argv[2], sys.argv[3])
else:
    printHelp()

